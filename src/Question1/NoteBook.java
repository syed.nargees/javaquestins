package Question1;

public class NoteBook extends Book {
	public void draw()
	{
		System.out.println("this pitcher have drawn by me");
	}
	
	@Override
	public void write() {
		System.out.println("this sentence is writen by nargees");
		
	}

	@Override
	public void read() {
		System.out.println("this book is read by nargees");
		
	}
	public static void main(String[] args) {
		NoteBook note=new NoteBook();
		note.write();
		note.read();
		note.draw();
		//Book book=new Book();//We cannot create objects of an abstract class.
	}

}
