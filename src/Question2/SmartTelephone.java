package Question2;

public class SmartTelephone extends Telephone{

	@Override
	public void with() {
		System.out.println("Telephone with");
	}

	@Override
	public void lift() {
		System.out.println("Telephone lift");
		
	}

	@Override
	public void disconnected() {
		System.out.println("Telephone disconnect");
		
	}

	public static void main(String[] args) {
		SmartTelephone smart=new SmartTelephone();
		smart.with();
		smart.lift();
		smart.disconnected();
	}

}
