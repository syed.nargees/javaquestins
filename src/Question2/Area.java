package Question2;

public class Area extends Shape {

double area;
@Override
void rectangleArea(int length, int breadth) {
	
	area = length * breadth;
	   System.out.println("Area of rectangle is: " + area);
}

@Override
void squareArea(int side) {
	area = side * side;
	   System.out.println("Area of Square is: " + area);
}

@Override
void circleArea(float radius) {
	area = (radius * radius) * 3.14;
	   System.out.println("Area of Circle is: " + area);
	
}
public static void main(String[] args) {
	Area a=new Area();
	a.rectangleArea(34, 65);
	a.squareArea(14);
	a.circleArea(45);
	
	
}

}
